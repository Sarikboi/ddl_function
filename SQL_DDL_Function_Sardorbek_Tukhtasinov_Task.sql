-- Dropping the existing view
DROP VIEW IF EXISTS sales_revenue_by_category_qtr;

-- Recreating the view
CREATE VIEW sales_revenue_by_category_qtr AS
SELECT
    fc.category_id,
    c.name AS category_name,
    SUM(p.amount) AS total_sales_revenue
FROM
    payment p
JOIN
    rental r ON p.rental_id = r.rental_id
JOIN
    inventory i ON r.inventory_id = i.inventory_id
JOIN
    film_category fc ON i.film_id = fc.film_id
JOIN
    category c ON fc.category_id = c.category_id
WHERE
    p.payment_date >= DATE_TRUNC('quarter', CURRENT_DATE)
GROUP BY
    fc.category_id, c.name
HAVING
    SUM(p.amount) > 0;


-- Create a query language function
CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_quarter VARCHAR(10))
RETURNS TABLE (
    category_id INT,
    category_name VARCHAR(255),
    total_sales_revenue DECIMAL(10, 2)
)
AS $$
DECLARE
    temp_result RECORD;
BEGIN
    -- Creating a temporary table to store the results
    CREATE TEMPORARY TABLE temp_result_table (
        category_id INT,
        category_name VARCHAR(255),
        total_sales_revenue DECIMAL(10, 2)
    );

    -- Iterating over the query results and insert into the temporary table
    FOR temp_result IN
        SELECT
            fc.category_id,
            c.name AS category_name,
            SUM(p.amount)::DECIMAL(10, 2) AS total_sales_revenue
        FROM
            payment p
        JOIN
            rental r ON p.rental_id = r.rental_id
        JOIN
            inventory i ON r.inventory_id = i.inventory_id
        JOIN
            film_category fc ON i.film_id = fc.film_id
        JOIN
            category c ON fc.category_id = c.category_id
        WHERE
            p.payment_date >= DATE_TRUNC('quarter', current_quarter::DATE)
        GROUP BY
            fc.category_id, c.name
        HAVING
            SUM(p.amount) > 0
    LOOP
        INSERT INTO temp_result_table VALUES (
            temp_result.category_id,
            temp_result.category_name,
            temp_result.total_sales_revenue
        );
    END LOOP;

    -- Returning the results from the temporary table
    RETURN QUERY SELECT * FROM temp_result_table;

    -- Dropping the temporary table
    DROP TABLE IF EXISTS temp_result_table;
END;
$$ LANGUAGE plpgsql;



-- Creating a procedure language function
CREATE OR REPLACE FUNCTION new_movie(movie_title VARCHAR)
RETURNS VOID
AS $$
BEGIN
    -- Inserting a new movie with the given title, setting language_id for 'Klingon'
    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (
        (SELECT COALESCE(MAX(film_id), 0) + 1 FROM film),  -- Generate a new unique film ID
        movie_title,
        4.99,
        3,
        19.99,
        EXTRACT(YEAR FROM CURRENT_DATE),
        (SELECT language_id FROM language WHERE name = 'Klingon')  -- Retrieve language_id for 'Klingon'
    );

    -- If the insertion is successful, print a message
    RAISE NOTICE 'New movie inserted: %', movie_title;
END;
$$ LANGUAGE plpgsql;


-- Creating a query language function called "get_language_id"
CREATE OR REPLACE FUNCTION get_language_id(language_name VARCHAR)
RETURNS INT
AS $$
DECLARE
    lang_id INT;
BEGIN
    -- Get the language_id for the specified language name
    SELECT language_id INTO lang_id FROM language WHERE name = language_name;
    RETURN lang_id;
END;
$$ LANGUAGE plpgsql;


